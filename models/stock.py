# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.addons.queue_job.job import job

import json
import csv


class StockLocationTag(models.Model):
    _description = 'Location Tags'
    _name = 'stock.location.tag'
    _order = 'name'

    name = fields.Char(string='Tag Name', required=True, translate=True)
    color = fields.Integer(string='Color Index')


class StockLocation(models.Model):
    _inherit = 'stock.location'
    sizex = fields.Integer('Depth (X)', default=0, help="Optional slot size details, for 3D representation only")
    sizey = fields.Integer('Width (Y)', default=0, help="Optional slot size details, for 3D representation only")
    sizez = fields.Integer('Height (Z)', default=0, help="Optional slot size details, for 3D representation only")
    
    planimetry_image = fields.Binary('Planimetry Image', help="A map used for 3D representation only (to be filled only for view locations)")
    
    camx = fields.Integer('Camera Position (X)', default=0, help="Optional position of the camera, for 3D representation only")
    camy = fields.Integer('Camera Position (Y)', default=0, help="Optional position of the camera, for 3D representation only")
    camz = fields.Integer('Camera Position (Z)', default=0, help="Optional position of the camera, for 3D representation only")
    
    camfov = fields.Integer('Camera Field of View', default=0, help="Optional field of view of the camera, for 3D representation only")
    
    planimetry_location_id = fields.Many2one(
        'stock.location', 'Planimetry Location', index=True, ondelete='set null',
        help="The location that contains basic information for 3D representation.")
    
    tag_ids = fields.Many2many(
        'stock.location.tag',
        column1='location_id',
        column2='tag_id',
        string='Tags')
        
# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import json

from odoo import http, tools, _
from odoo.http import request


class ThreeDViewController(http.Controller):

    @http.route(
        '/3dview/get_locations', type='json', auth="user", methods=['POST'])
    def get_locations(self, domain, **kwargs):
        locations = request.env['stock.location'].search(domain)
        vals = []
        for location in locations:
            vals.append( {
                'posx': location['posx'],
                'posy': location['posy'],
                'posz': location['posz'],
                'sizex': location['sizex'],
                'sizey': location['sizey'],
                'sizez': location['sizez'],
                'opacity': 200,
                'barcode' : location.barcode,
                'color': 'gray',
                'type': 'big',
                })

        return json.dumps(vals)

    @http.route(
        '/3dview/get_scene', type='json', auth="user", methods=['POST'])
    def get_scene(self, domain, **kwargs):
        
        data = {}

        # first, we retrieve the information about the camera
        locations = request.env['stock.location'].search(domain)
        
        data['camera'] = {  
                'camx': locations[0]['camx'],
                'camy': locations[0]['camy'],
                'camz': locations[0]['camz'],
                'camfov': locations[0]['camfov'],
            }
        
        # second, we retrieve the information about the planimetry
        locations = request.env['stock.location'].search( [('id', '=', locations[0]['planimetry_location_id'].id)] )

        data['ground'] = {  
                'sizex': locations[0]['sizex'],
                'sizey': locations[0]['sizey'],
                'sizez': locations[0]['sizez'],
                'planimetry_image': locations[0]['planimetry_image']
            }
        
        return json.dumps(data)

    @http.route(
        '/3dview/get_info', type='json', auth="user", methods=['POST'])
    def get_info(self, domain, **kwargs):
        locations = request.env['stock.location'].search(domain)

        return json.dumps({  
                'barcode': locations[0]['barcode'],
                'id': locations[0]['id']
            })

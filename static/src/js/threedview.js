odoo.define('stock_3dview.3DView', function (require) {
"use strict";

var ajax = require('web.ajax');
var core = require('web.core');
var View = require('web.View');
var data_manager = require('web.data_manager');

var _lt = core._lt;
var QWeb = core.qweb;


var ThreeDView = View.extend({
    display_name: _lt('ThreeD View'),
    icon: 'fa-cubes',
    template: '3DView',
    view_type: 'threedview',

    init: function (parent, context) {
        var self = this;
        this._super.apply(this, arguments);
        this.has_been_loaded = $.Deferred();        
    },
    do_search: function (domains, contexts, group_bys) {
        var self = this;
        var ajax = require('web.ajax');
        try {
            loadLocations(ajax, domains);
        }
        catch(err) {
            // not ready yet
        }
        
        return this._super.apply(this, arguments);
    },
});

core.view_registry.add('threedview', ThreeDView);

return ThreeDView;
});

odoo.define('stock.threedview', function (require) {
    "use strict";

    if ( ! Detector.webgl ) Detector.addGetWebGLMessage();

    function realTo3dvSizes (x, y, z) {
        return [x / scale, z / scale, y / scale];
    }

    function realTo3dvCoords (x, y, z, sizex, sizey, sizez) {
        return [ (x + sizex/2) / scale, (z + sizez/2) / scale, - (y +sizey/2) / scale];
    }

    function init(gd) {
        
        console.log("initializing scene...");

        console.log('fov: ' + refSys.camFov);

        camera = new THREE.PerspectiveCamera( refSys.camFov, window.innerWidth / window.innerHeight, 1, 2000 );
        // TODO check whether we should use the size of the container
        camera.position.fromArray( realTo3dvSizes( refSys.camPosX_real, -refSys.camPosY_real, refSys.camPosZ_real ) );

        controls = new THREE.OrbitControls( camera, document.getElementById( '3dview_container' ) );

        controls.rotateSpeed = 1.0;
        controls.zoomSpeed = 1.2;
        controls.panSpeed = 0.8;

        controls.enableZoom = true;
        controls.enablePan = true;

        controls.enableDamping = true;
        controls.dampingFactor = 0.3;

        //controls.keys = [ 65, 83, 68 ];

        controls.addEventListener( 'change', render );
                    
        raycaster = new THREE.Raycaster();
        mouse = new THREE.Vector2();

        // world

        scene = new THREE.Scene();
        //scene.fog = new THREE.FogExp2( 0xcccccc, 0.001 );

        var grid = new THREE.GridHelper(2000, 50);
        grid.position.x=refSys.groundX_3dv/2;
        grid.position.y=-1;
        grid.position.z=-refSys.groundZ_3dv/2;
        scene.add(grid);
        
        target = new THREE.Mesh(
            new THREE.BoxGeometry( 10, 10, 10 ),
            new THREE.MeshPhongMaterial( { color: new THREE.Color("red"), opacity: 1, transparent: true } )
        );
        
        target.position.fromArray( realTo3dvSizes( refSys.targetX_real, -refSys.targetY_real, refSys.targetZ_real ) );
        
        target.name = "target";
        
        scene.add(target);
        
        //controls.reset();

        camera.updateProjectionMatrix();
                        
        saveCameraSettings();

        // var axisHelper = new THREE.AxisHelper( 5 );
        // scene.add( axisHelper );

        /*
        gui = new dat.GUI({ width : 300, autoPlace: false });
        
        var guiCameraFolder = gui.addFolder("Camera position");

        guiCameraFolder.add(refSys, 'camPosX_real', -refSys.groundX_real*0.5, refSys.groundX_real*1.5).name("x").step(100).listen();
        guiCameraFolder.add(refSys, 'camPosY_real', -refSys.groundY_real*0.5, refSys.groundY_real*1.5).name("y").step(100).listen();
        guiCameraFolder.add(refSys, 'camPosZ_real', 0, refSys.heightZ_real*1.5).name("z").step(100).listen();

        var guiCameraParamsFolder = gui.addFolder("Camera field of view");
        guiCameraParamsFolder.add(camera, 'fov', 10, 100).step(1);

        var guiCameraTargetFolder = gui.addFolder("Camera target");

        guiCameraTargetFolder.add(refSys, 'targetX_real', 0, refSys.groundX_real).name("x").step(100).listen();
        guiCameraTargetFolder.add(refSys, 'targetY_real', 0, refSys.groundY_real).name("y").step(100).listen();
        guiCameraTargetFolder.add(refSys, 'targetZ_real', 0, refSys.heightZ_real).name("z").step(100).listen();
        guiCameraTargetFolder.add(target.material, 'visible');

        //dat.GUI.toggleHide();
        
        //$('.o_content').append(gui.domElement);
        */


        var image = document.createElement( 'img' );
        var texture = new THREE.Texture( image );
        image.onload = function()  {
            texture.needsUpdate = true;
        };
        
        image.src = "data:image/png;base64," +gd['planimetry_image'];

        texture.wrapS = THREE.ClampToEdgeWrapping;
        texture.wrapT = THREE.ClampToEdgeWrapping;
        
        //var sizes = realTo3dvSizes (box_realx, box_realy, box_realz);

        //var groundGeometry = new THREE.BoxGeometry( refSys.groundX_3dv*100, 1, refSys.groundY_3dv*100 );

        var groundGeometry = new THREE.BoxGeometry( refSys.groundX_3dv, 2, refSys.groundZ_3dv );
        
        var groundMaterial = new THREE.MeshPhongMaterial( { map: texture, shading: THREE.FlatShading } );
        
        var ground = new THREE.Mesh( groundGeometry, groundMaterial );
        
        ground.position.fromArray(realTo3dvCoords (0, 0, 0, refSys.groundX_real, refSys.groundY_real, 0)).y=-1;
        
        var underground= new THREE.Mesh(
            new THREE.BoxGeometry( refSys.groundX_3dv, 2, refSys.groundZ_3dv ),
            new THREE.MeshPhongMaterial( { color: new THREE.Color("gray"), opacity: 1, transparent: false } )
        );
        
        underground.position.fromArray(realTo3dvCoords (0, 0, 0, refSys.groundX_real, refSys.groundY_real, 0)).y=-3;

        scene.add( ground );
        scene.add( underground );

         
        // lights

        var light = new THREE.DirectionalLight( 0xffffff );
        light.position.set( 1, 1, 1 );
        scene.add( light );

        var light = new THREE.DirectionalLight( 0xffffff );
        light.position.set( -1, -1, -1 );
        scene.add( light );

        var light = new THREE.AmbientLight( 0x606060 );
        scene.add( light );

        // renderer

        renderer = new THREE.WebGLRenderer( { antialias: false } );
        renderer.setClearColor( 0xf0f0f0 );
        renderer.setPixelRatio( window.devicePixelRatio );
        setRendererSize();

        container = document.getElementById( '3dview_container' );
        container.appendChild( renderer.domElement );
        
        /*
        stats = new Stats();
        container.appendChild( stats.dom );
        */
        //

        
        //container.addEventListener( 'click', onDocumentMouseDown, false );
        $( container ).dblclick( onContainerDoubleClick );
        
        //container.addEventListener( 'touchstart', onDocumentTouchStart, false );

        window.addEventListener( 'resize', onWindowResize, false );
        
        //controls.reset();
        
        camera.lookAt(target);
        
        camera.userData.scene = scene;

        controls.target.copy(target.position);
        
        console.log("scene:");
        console.log(camera.userData.scene);
        
        controls.update();
        
        
    }

    function removeObject( name ) {
        var object = scene.getObjectByName( name );
        if ( object ) {
            scene.remove( object );
            if (object.geometry) object.geometry.dispose();
            if (object.material) object.material.dispose();
        }
    }

    function removeLocations() {
        // before adding new locations, we might need to remove the old ones...
        for (var i=0; i<objects.length; i++)
        {
            removeObject( objects[i].name );
            removeObject( 'wireframe' + objects[i].name );
        }
        objects = [];
    }

    function loadLocations(parent_location_id) {

        $("#reload_icon").addClass("fa-spin").removeClass("clickable");
        
        var domain = [[ 'location_id', '=', parent_location_id ]];

        ajax.jsonRpc("/3dview/get_locations", 'call', {'domain': domain }).then( function( locations_data ) {

            var ld = JSON.parse(locations_data);

            console.log("Found " + ld.length + " locations");
            console.log("loading locations...");

            for (var i=0; i<ld.length; i++)
            {
                var location = ld[i];

                var size = realTo3dvSizes (location.sizex, location.sizey, location.sizez);
               
                var geometry = new THREE.BoxGeometry( size[0], size[1], size[2] );
                
                var material = new THREE.MeshPhongMaterial( { color: new THREE.Color(location.color), shading: THREE.FlatShading, opacity: parseInt(location.opacity)/1000, transparent: true } );
                var cube = new THREE.Mesh( geometry, material );
                
                // wireframe
                var geo = new THREE.EdgesGeometry( geometry ); // or WireframeGeometry
                var mat = new THREE.LineBasicMaterial( { color: 0x000000, linewidth: 1 } );
                var wireframe = new THREE.LineSegments( geo, mat );
                
                cube.position.fromArray( realTo3dvCoords ( 
                    parseInt(location.posx),
                    parseInt(location.posy),
                    parseInt(location.posz),
                    parseInt(location.sizex),
                    parseInt(location.sizey),
                    parseInt(location.sizez)
                    ));
                wireframe.position.copy( cube.position );
                
                cube.userData.barcode = location.barcode;
                cube.userData.originalColor = location.color;

                cube.name = location.barcode;
                wireframe.name = 'wireframe' + location.barcode;
                
                scene.add( cube );
                scene.add( wireframe );
                objects.push ( cube );
               
            }
        $("#reload_icon").removeClass("fa-spin").attr("title", "Click to reload").addClass("clickable");

        })
    }

    function animate() {

        requestAnimationFrame( animate );
        //camera.up = new THREE.Vector3(0,1,0);
        
        /*
        var isGuiVisible = $(".dg.a").is(":visible");
        controls.enabled = ! isGuiVisible;
        
        if (isGuiVisible) {
            
            camera.position.fromArray( realTo3dvSizes( refSys.camPosX_real, -refSys.camPosY_real, refSys.camPosZ_real ) );
            
            target.position.fromArray( realTo3dvSizes( refSys.targetX_real, -refSys.targetY_real, refSys.targetZ_real ) );
            
            camera.lookAt( target.position );
            console.log("updated lookAt...");
            
            saveCameraSettings();
            //camera.rotation.y = refSys.camRotX * Math.PI / 180
            //camera.rotateZ( refSys.camRotX );
            //console.log(refSys.camRotX);
            //camera.updateProjectionMatrix();
            
            cs = 1;
            
            $('#cross_container').hide();

        }
        else {
            if ( cs == 1 ) {
                restoreCameraSettings();
                cs = 2;
                console.log ( "camera settings restored" );
            }
            
            
            
            $('#cross_container')
                .css('top', ($('#container').height()/2 -16)+'px' )
                .css('left', ($('#container').width()/2 -16)+'px' )
                .show();

            controls.update();
            
            //camera.rotation.x =0;

            refSys.camPosX_real = camera.position.x * scale;
            refSys.camPosY_real = - camera.position.z * scale;
            refSys.camPosZ_real = camera.position.y * scale;
            refSys.targetX_real = target.position.x * scale;
            refSys.targetY_real = - target.position.z * scale;
            refSys.targetZ_real = target.position.y * scale;
            

        }
        
        //console.log(controls.enabled);
        
        //camera.rotation.x -= 0.1; 
        //console.log("rotated" + camera.rotation.x);

        // camera.lookAt( target.position );
        camera.updateProjectionMatrix();
        
        
        
        updateInfoBox();
        
        
        //console.log(camera.position.x);
        //camera.lookAt(scene.position);
        //camera.updateProjectionMatrix();

        */
        render();

    }

    function render() {

        target.position.copy(controls.target);

        renderer.render( scene, camera );
        //stats.update();

    }

                
    function saveCameraSettings() {
        cameraSettings.position = camera.position.clone();
        cameraSettings.rotation = camera.rotation.clone();
        cameraSettings.controlCenter = controls.target.clone();
    }

    function restoreCameraSettings() {
        camera.position.set(
            cameraSettings.position.x, 
            cameraSettings.position.y, 
            cameraSettings.position.z );
            
        camera.rotation.set(
            cameraSettings.rotation.x, 
            cameraSettings.rotation.y, 
            cameraSettings.rotation.z );

        controls.target.set(
            cameraSettings.controlCenter.x, 
            cameraSettings.controlCenter.y, 
            cameraSettings.controlCenter.z );
            
        controls.update();
    }


    // event handling

            
    function onContainerTouchStart( event ) {

        event.preventDefault();

        event.clientX = event.touches[0].clientX;
        event.clientY = event.touches[0].clientY;
        onDocumentMouseDown( event );

    }

    function onContainerDoubleClick( event ) {
        
        if ( $('#location_info').data('current')!==0 )
        {
            return;
        }

        event.preventDefault();

        mouse.x = ( (event.clientX - $('#3dview_container').offset().left -10 ) / renderer.domElement.clientWidth ) * 2 - 1;
        mouse.y = - ( (event.clientY - $('#3dview_container').offset().top -10) / renderer.domElement.clientHeight ) * 2 + 1;

        raycaster.setFromCamera( mouse, camera );

        var intersects = raycaster.intersectObjects( objects );

        if ( intersects.length > 0 ) {
            
            var location = intersects[ 0 ].object;
            console.log(location.uuid);
            location.material.color.setHex( 0xffff00 );
            
            $("#location_info")
                .css({ 'top': event.clientY - $('#3dview_container').offset().top - 10, 'left': event.clientX - $('#3dview_container').offset().left - 10})
                .text(location.userData.id + ", retrieving info...")
                .data('current', location.userData.barcode)
                .show()
                ;
            
            var domain = [['barcode', '=', location.userData.barcode]]; 
            ajax.jsonRpc("/3dview/get_info", 'call', {'domain': domain }).then( function (data) {
                var info = JSON.parse(data);
                $("#location_info").text(location.userData.barcode + ' (id: ' + info['id'] +')');
                } );
            
            controls.enabled = false;
        }
    }

    function onWindowResize() {

        setRendererSize();
        // controls.handleResize();

        render();

    }

    function setRendererSize() {

        var width;
        var height;

        if ( $("#3dview_container").hasClass( "fullscreen" ) ) {
            width = $(document).width();
            height = $(document).height();
        } else {
            width = $('.o_content').width();
            height = $('.o_content').height();
        }
        
        camera.aspect = width / height;
        camera.updateProjectionMatrix();

        if ( $("#3dview_container").hasClass( "fullscreen" ) ) {
            renderer.setSize( width, height );
        } else {
            renderer.setSize( width -20 , height -20 );
        }
    }

    $("#location_info").click(function(e) {
        e.preventDefault();
        var location = scene.getObjectByName( $("#location_info").data("current") );
        
        if ( location )
        {
            location.material.color = new THREE.Color(location.userData.originalColor);
        }
        
        $("#location_info").data("current", 0).hide();

        controls.enabled = true;
        e.stopPropagation();
    })

    $("#reload_icon").click(function(e) {
        e.preventDefault();
        if ( !$("#reload_icon").hasClass('clickable') )
        {
            return;
        }
        removeLocations();
        loadLocations( parent_location_id );
    })

    $("#fullscreen_icon").click(function(e) {
        e.preventDefault();
        $("#3dview_container").toggleClass("fullscreen");
        setRendererSize();
    })

    /*
    $(document).keydown(function(e){
        
        var code = e.keyCode || e.which;
        if (code==70 || code==102) { // F key
            $("#3dview_container").addClass("fullscreen");
        }
        if (code==27) { // escape key
            $("#3dview_container").removeClass("fullscreen");
        }
        if (code==69 || code == 101 ) { // E key
            controls.reset();
            //camera.rotation.set(0,0,0);
        }
        
        var m = 100;
        
        if (code==37) {  // left
            refSys.camPosY_real -=m;
        }
        if (code==38) {  // up
            refSys.camPosX_real +=m;
        }
        if (code==39) {  // rigth
            refSys.camPosY_real +=m;
        }
        if (code==40) {  // down
            refSys.camPosX_real -=m;
        }
        

        if (code==72 || code==104 ) { // H key
            camera.up = new THREE.Vector3(0,1,0);
        }
        
        
    });
    */
                
                
    // for mobile devices, where no "H" key is available...
    /*
    $(document).on('doubletap', function(e) {
            if ($(".dg.a").is(":visible")) {
                $(".dg.a").hide();
            }
            else {
                $(".dg.a").show();
            }
    });
    */


    // main code


    var refSys;
    var scale;

    var container, stats;

    var camera, controls, scene, renderer;

    var target;

    var cross;

    var gui;

    var cameraSettings = {};

    var cs = 1; // with GUI;

    var objects = [];
    
    var raycaster;
    
    var mouse;
    
    var parent_location_id = 9811;  // TODO how to get it from odoo?

    var Model = require('web.Model');
    var stock_location = new  Model('stock.location');
    var ajax = require('web.ajax');
    // loadLocations(ajax, []);

    console.log("retrieving data...");
    
    var domain = [['id', '=', parent_location_id]];
    
    var scene_call = ajax.jsonRpc("/3dview/get_scene", 'call', {'domain':  domain });
    //var locations_call = ajax.jsonRpc("/3dview/get_locations", 'call', {'domain': domain });

    $.when(scene_call).done(function(scene_data) {
        console.log("processing data...");
        var sd = JSON.parse(scene_data);

        refSys = {
            groundX_real: sd['ground']['sizex'],
            groundY_real: sd['ground']['sizey'],
            heightZ_real: sd['ground']['sizez'],
            camPosX_real: sd['camera']['camx'],
            camPosY_real: sd['camera']['camy'],
            camPosZ_real: sd['camera']['camz'],
            camFov: sd['camera']['camfov']
        }

        refSys.targetX_real = sd['ground']['sizex'] / 2;
        refSys.targetY_real = sd['ground']['sizey'] / 2;
        refSys.targetZ_real = 0;

        refSys.groundX_3dv = 600;
        refSys.groundZ_3dv = Math.round( refSys.groundY_real / refSys.groundX_real * refSys.groundX_3dv );
        refSys.heightY_3dv = Math.round( refSys.heightZ_real / refSys.groundX_real * refSys.groundX_3dv );

        //console.log (refSys);

        scale = refSys.groundX_real / refSys.groundX_3dv;
        
        init(sd['ground']);
        $("#reload_icon").removeClass("fa-spin").addClass("clickable");
        removeLocations();
        loadLocations(parent_location_id);
        animate();

    })
    
    //setInterval(removeLocations, 5000);  // used to test the removal...

    
});
